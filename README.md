# Analytics developer take-home challenge

First off, thanks for taking the time and creating a solution for take-home challenge! If you have any questions, you need any further instruction, encountered to a problem feel free to shoot us an email to datateam@liligo.com


We provide to you the development environment with Docker. You don't have to be familiar with it but you have to install and run it in order to complete this take-home challenge.


You can access this environment on the link below and please follow the steps to install and set up Docker.


1. git clone https://gitlab.com/liligo/analytics-developer-take-home-challenge
2. Please install Docker and docker-compose and make sure you try it out before you start! https://docs.docker.com/get-started/
3. Within the repository directory hit `docker-compose up`


## The challenge

For completing this task you can use your preferred programming language.

If you check what's inside the docker-compose.yml file you will see there 2 databases. A MySQL and a Postgres. Also you find the credentials in the docker-compose file. You can connect to each on localhost(or 127.0.0.1) and the port defined in the file.

The MySQL contains a liligo database and a liligo table inside. It contains events from a fictional web product.

The liligo table looks like this:
* `index` - numeric - it's not a proper index or id but probably there are no duplicated values so you can use it for counting purposes if you want.
* `timestamp` - datetime - the event's timestamp
* `metric` - string - The metrics is a web page event let's say that actually common in any digital product. For completing the task you don't have to understand the meaning of these events. The values can be `page_view`,`click_button`, and `click-out`.


You have to complete 2 simple tasks.

1. Aggregate how many times the metrics occurred on daily basis. Save the aggregated results to a table in the provided Postgres database.
2. Using any framework or tool create a visualization from the results you've saved to Postgres. (e.g.: a line chart with values per metric and day.)

You get free hands for what will be your approach to complete this. Think about cases like what will happen if you re-run your solution, we add more data to MySQL, we introduce more metrics, or you name it. We would like to see a solution where you highlight your skills, experience and ideas. We are expecting code as a solution for both parts, instructions how to run your code, minimal documentation, and a git repository containing all the things you done. Also we'd like to ask you don't spend more than 8 hours with the task as others may don't have much more time to work on it.


You can submit your solution via email as a file or just as link to your repository.
